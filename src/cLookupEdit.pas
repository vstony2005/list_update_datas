unit cLookupEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cEditCompletion, Data.DB, Vcl.Grids,
  Vcl.DBGrids, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TFraLookupEdit = class(TFraEditCompletion)
  private
    FDataSource: TDataSource;
    FFieldName: string;
    FTableRef: string;
    FIsInit: Boolean;
    procedure OnLkEditIdChange(Sender: TObject);
    procedure SetTableRef(const Value: string);
  public
    constructor Create(AOwner: TComponent); override;
    procedure InitId(AId: Variant);
    property DataSource: TDataSource read FDataSource write FDataSource;
    property FieldName: string read FFieldName write FFieldName;
    property TableRef: string read FTableRef write SetTableRef;
  end;

implementation

{$R *.dfm}

{ TFraLookupEdit }

constructor TFraLookupEdit.Create(AOwner: TComponent);
begin
  inherited;
  OnIdChange := OnLkEditIdChange;
end;

procedure TFraLookupEdit.InitId(AId: Variant);
begin
  FIsInit := True;
  Id := AId;
  FIsInit := False;
end;

procedure TFraLookupEdit.OnLkEditIdChange(Sender: TObject);
begin
  if Assigned(DataSource)
    and (FieldName <> '')
    and (not FIsInit) then
  begin
    if not (DataSource.State in [dsInsert,dsEdit]) then
      DataSource.Edit;
    DataSource.DataSet[FieldName] := Id;
  end
  else
  begin
    // ERROR !!
  end;
end;

procedure TFraLookupEdit.SetTableRef(const Value: string);
begin
  if (FTableRef = Value) then
    Exit;

  FTableRef := Value;

  if (FTableRef = 'Customers') then
  begin
    Init('Customers','CustomerID','CompanyName','CompanyName',
         'CustomerID,CompanyName','CompanyName');
    SetLabel('Customer');
  end
  else if (FTableRef = 'Categories') then
  begin
    Init('Categories','CategoryID','CategoryName','CategoryName',
         'CategoryID,CategoryName','CategoryName');
    SetLabel('Category');
  end
  else if (FTableRef = 'Shippers') then
  begin
    Init('Shippers','ShipperID','CompanyName','CompanyName',
         'ShipperID,CompanyName','CompanyName');
    SetLabel('Shipper');
  end
  else if (FTableRef = 'Products') then
  begin
    Init('Products','ProductID','ProductName','ProductName',
         'ProductID,ProductName','ProductName');
    SetLabel('Product');
  end
  else if (FTableRef = 'Suppliers') then
  begin
    Init('Suppliers','SupplierID','CompanyName','CompanyName',
         'SupplierID,CompanyName','CompanyName');
    SetLabel('Supplier');
  end
  else if (FTableRef = 'Employees') then
  begin
    Init('EmployeesFirstLastName','EmployeeID','FirstLastName','FirstLastName',
         'EmployeeID,FirstName,LastName','LastName,FirstName');
    SetLabel('Employee');
  end;
end;

end.
