object FrmEditDatas: TFrmEditDatas
  Left = 0
  Top = 0
  Caption = 'FrmEditDatas'
  ClientHeight = 441
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  Position = poMainFormCenter
  OnCreate = FormCreate
  TextHeight = 15
  object lblProduct: TLabel
    Left = 80
    Top = 19
    Width = 42
    Height = 15
    Caption = 'Product'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = [fsUnderline]
    ParentFont = False
    OnClick = lblProductClick
  end
  object lblSearch: TLabel
    Left = 80
    Top = 352
    Width = 48
    Height = 15
    Caption = 'lblSearch'
  end
  object Label1: TLabel
    Left = 80
    Top = 294
    Width = 35
    Height = 15
    Caption = 'Search'
  end
  object edt1: TJvEdit
    Left = 80
    Top = 40
    Width = 361
    Height = 23
    TabOrder = 0
    Text = 'edt1'
    OnChange = edt1Change
    OnExit = edt1Exit
    OnKeyDown = edt1KeyDown
  end
  object Grid1: TJvDBGrid
    Left = 192
    Top = 104
    Width = 320
    Height = 120
    DataSource = ds1
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 19
    TitleRowHeight = 19
  end
  object edtSearch: TJvEdit
    Left = 80
    Top = 315
    Width = 361
    Height = 23
    TabOrder = 2
    Text = ''
    OnChange = edtSearchChange
    TextHint = 'Search...'
  end
  object ds1: TDataSource
    OnDataChange = ds1DataChange
    Left = 296
    Top = 136
  end
end
