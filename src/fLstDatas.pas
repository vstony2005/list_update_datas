unit fLstDatas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.ExtCtrls, Vcl.DBCtrls,
  Vcl.Grids, Vcl.DBGrids, Datasnap.Provider, Datasnap.DBClient, JvExDBGrids,
  JvDBGrid, JvDataSource, Vcl.StdCtrls, JvExStdCtrls, JvEdit,
  Vcl.Samples.Spin, Firedac.Comp.Client,
  uParamCols;

type
  TMyDBGrid = class(TDBGrid);
  TMyQry = class(TFDQuery);
  TFrmLstDatas = class(TForm)
    ds1: TDataSource;
    DBNavigator1: TDBNavigator;
    JvDBGrid1: TJvDBGrid;
    edtSearch: TJvEdit;
    lblNbLines: TLabel;
    btnPlus: TButton;
    btnMoins: TButton;
    btnGo: TButton;
    edtGoLine: TSpinEdit;
    lblPosition: TLabel;
    procedure btnMoinsClick(Sender: TObject);
    procedure btnPlusClick(Sender: TObject);
    procedure btnGoClick(Sender: TObject);
    procedure edtGoLineKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormDestroy(Sender: TObject);
    procedure edtSearchChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure JvDBGrid1AfterPaint(Sender: TObject);
    procedure JvDBGrid1GetBtnParams(Sender: TObject; Field: TField; AFont: TFont;
        var Background: TColor; var ASortMarker: TSortMarker; IsDown: Boolean);
    procedure JvDBGrid1TitleClick(Column: TColumn);
  private
    FIsAsc: Boolean;
    FTableCols: TTableCols;
    FQry: TFDQuery;
    FTableName: string;
    procedure FQryAfterOpen(DataSet: TDataSet);
    procedure MoveBy(AValue: Integer);
    function IndexOfCol(const AColName: string): Integer;
    procedure SetTableName(const Value: string);
  public
    property TableName: string read FTableName write SetTableName;
  end;

implementation

{$R *.dfm}

uses
  FireDAC.Comp.DataSet, FireDAC.Stan.Intf,
  dmDatas;

procedure TFrmLstDatas.FormCreate(Sender: TObject);
begin
  FTableCols := TTableCols.Create;
  JvDBGrid1.TitleButtons := True;
end;

procedure TFrmLstDatas.FormDestroy(Sender: TObject);
begin
  FTableCols.Free;
end;

procedure TFrmLstDatas.SetTableName(const Value: string);
begin
  if (FTableName = Value) then Exit;

  FTableName := Value;

  FTableCols.TableName := FTableName;

  if Assigned(FQry) then
  begin
    FQry.Free;
    FQry := nil;
  end;

  FQry := MDatas.GetQuery(Self);
  FQry.SQL.Text := FTableCols.GetSql;

  FQry.AfterOpen := FQryAfterOpen;

  ds1.DataSet := FQry; // avant le open
  FQry.Open;
end;

{$REGION 'Buttons'}
procedure TFrmLstDatas.btnPlusClick(Sender: TObject);
begin
  MoveBy(TMyDBGrid(JvDBGrid1).VisibleRowCount);
end;

procedure TFrmLstDatas.btnMoinsClick(Sender: TObject);
begin
  MoveBy(-TMyDBGrid(JvDBGrid1).VisibleRowCount);
end;

procedure TFrmLstDatas.btnGoClick(Sender: TObject);
begin
  if not Assigned(FQry) then Exit;

  if (edtGoLine.Value > 0) then
  begin
    FQry.First;
    MoveBy(edtGoLine.Value);
  end;
end;
{$ENDREGION}

procedure TFrmLstDatas.edtSearchChange(Sender: TObject);
begin
  if not Assigned(FQry) then Exit;

  if (JvDBGrid1.SortedField <> string.Empty) then
  begin
    FQry.Filtered := False;
    FQry.FilterOptions := [foCaseInsensitive,foNoPartialCompare];
    FQry.Filter := Format('%s LIKE ''%%%s%%''',
                          [JvDBGrid1.SortedField,
                           edtSearch.Text]);
    FQry.Filtered := True;
  end;
end;

procedure TFrmLstDatas.FQryAfterOpen(DataSet: TDataSet);
begin
  FTableCols.PrepareDBGrid(JvDBGrid1.Columns);
end;

procedure TFrmLstDatas.FormKeyPress(Sender: TObject; var Key: Char);
begin
  // Self.KeyPreview := True;
//  if (edtGoLine.Focused) and (Key = #13) then
//    btnGoClick(btnGo);
end;

procedure TFrmLstDatas.edtGoLineKeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
begin
  if (Key = VK_RETURN) then
    btnGoClick(btnGo);
end;

{$REGION 'DBGrid'}
procedure TFrmLstDatas.JvDBGrid1TitleClick(Column: TColumn);
var
  col, cold: string;
begin
  if not Assigned(FQry) then Exit;

  col := Column.FieldName;
  cold := Format('%s:D', [col]);

  if FQry.IndexFieldNames = col then
  begin
    FQry.IndexFieldNames := cold;
    FIsAsc := False;
  end
  else
  begin
    FQry.IndexFieldNames := col;
    FIsAsc := True;
  end;

  JvDBGrid1.SortedField := col;

  if (edtSearch.Text <> string.Empty) then
    edtSearchChange(edtSearch);
end;

procedure TFrmLstDatas.JvDBGrid1GetBtnParams(Sender: TObject; Field: TField;
    AFont: TFont; var Background: TColor; var ASortMarker: TSortMarker; IsDown:
    Boolean);
const
  Direction: array[boolean] of TSortmarker = (smDown, smUp);
begin
  if (Field.FieldName = JvDBGrid1.SortedField) then
    ASortMarker := Direction[FIsAsc]
  else
    ASortMarker := smNone;
end;

procedure TFrmLstDatas.JvDBGrid1AfterPaint(Sender: TObject);
var
  nb, row, pos: Integer;
begin
  if not Assigned(FQry) then Exit;

  // nb lines visible in dbgrid
  nb := TMyDBGrid(JvDBGrid1).VisibleRowCount;
  // current line in dbgrid
  row := TMyDBGrid(JvDBGrid1).Row;
  lblNbLines.Caption := Format('Nb Lines: %d / Row: %d', [nb, row]);

  // current line in cds
  pos := TMyQry(FQry).GetRecNo;
  pos := 0;
  lblPosition.Caption := Format('Position: %d (%d-%d)',
                                [pos,
                                 pos-row+1,
                                 pos-row+nb]);
end;
{$ENDREGION}

function TFrmLstDatas.IndexOfCol(const AColName: string): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to JvDBGrid1.Columns.Count - 1 do
  begin
    if (JvDBGrid1.Columns[i].FieldName = AColName) then
    begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrmLstDatas.MoveBy(AValue: Integer);
begin
  if not Assigned(FQry) then Exit;

  FQry.MoveBy(AValue);
end;

end.
