object FrmMajDatas: TFrmMajDatas
  Left = 0
  Top = 0
  Caption = 'Update Datas'
  ClientHeight = 516
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  Position = poMainFormCenter
  OnCreate = FormCreate
  DesignSize = (
    624
    516)
  TextHeight = 15
  object Label1: TLabel
    Left = 25
    Top = 307
    Width = 34
    Height = 15
    Caption = 'Label1'
  end
  object Label2: TLabel
    Left = 25
    Top = 490
    Width = 96
    Height = 18
    Anchors = [akLeft, akBottom]
    Caption = 'Label2'
  end
  object DBGrid1: TDBGrid
    Left = 16
    Top = 16
    Width = 585
    Height = 273
    Anchors = [akLeft, akTop, akRight]
    DataSource = ds1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
  end
  object DBEdit1: TDBEdit
    Left = 89
    Top = 304
    Width = 368
    Height = 23
    DataSource = ds1
    TabOrder = 1
  end
  inline FraCatgory: TFraLookupEdit
    Left = 16
    Top = 333
    Width = 464
    Height = 104
    TabOrder = 2
    ExplicitLeft = 16
    ExplicitTop = 333
    ExplicitHeight = 104
  end
  object ds1: TDataSource
    DataSet = cds1
    OnDataChange = ds1DataChange
    Left = 512
    Top = 304
  end
  object dsp1: TDataSetProvider
    Left = 512
    Top = 416
  end
  object cds1: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dsp1'
    Left = 512
    Top = 360
  end
end
