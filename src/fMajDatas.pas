unit fMajDatas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Datasnap.DBClient,
  Datasnap.Provider, cEditCompletion, cLookupEdit, Vcl.StdCtrls, Vcl.Mask,
  Vcl.ExtCtrls, Vcl.DBCtrls, Vcl.Grids, Vcl.DBGrids;

type
  TFrmMajDatas = class(TForm)
    DBGrid1: TDBGrid;
    DBEdit1: TDBEdit;
    Label1: TLabel;
    FraCatgory: TFraLookupEdit;
    ds1: TDataSource;
    dsp1: TDataSetProvider;
    cds1: TClientDataSet;
    Label2: TLabel;
    procedure ds1DataChange(Sender: TObject; Field: TField);
    procedure FormCreate(Sender: TObject);
  private
  public
  end;

implementation

{$R *.dfm}

uses
  FireDAC.Comp.Client,
  dmDatas;

procedure TFrmMajDatas.ds1DataChange(Sender: TObject; Field: TField);
begin
  if (cds1.State <> dsInactive) then
  begin
    Label2.Caption := Format('Pid: %d Cid : %d',
                             [cds1.FieldByName('ProductID').AsInteger,
                              cds1.FieldByName('CategoryID').AsInteger]);
    FraCatgory.InitId(cds1.FieldByName('CategoryID').AsInteger);
  end;
end;

procedure TFrmMajDatas.FormCreate(Sender: TObject);
var
  qry: TFDQuery;
begin
  FraCatgory.TableRef := 'Categories';
  FraCatgory.FieldName := 'CategoryID';
  FraCatgory.DataSource := ds1;

  qry := MDatas.GetQuery(Self);
  qry.SQL.Add('SELECT ProductID, ProductName, CategoryID '
              + 'FROM Products ORDER BY ProductName');
  dsp1.DataSet := qry;
  cds1.Open;
end;

end.
