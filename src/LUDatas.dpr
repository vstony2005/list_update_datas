program LUDatas;

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Vcl.Forms,
  fMain in 'fMain.pas' {FrmMain},
  dmDatas in 'dmDatas.pas' {MDatas: TDataModule},
  fBaseList in 'fBaseList.pas' {FrmBaseList},
  fLstDatas in 'fLstDatas.pas' {FrmLstDatas},
  fSelDatas in 'fSelDatas.pas' {FrmSelDatas},
  uMyMessages in 'uMyMessages.pas',
  uMyTools in 'uMyTools.pas',
  uParamCols in 'uParamCols.pas',
  fEditDatas in 'fEditDatas.pas' {FrmEditDatas},
  cShowGrid in 'cShowGrid.pas' {FraShowGrid: TFrame},
  cEditCompletion in 'cEditCompletion.pas' {FraEditCompletion: TFrame},
  fTestAutoCompletion in 'fTestAutoCompletion.pas' {FrmTestAutoCompl},
  cLookupEdit in 'cLookupEdit.pas' {FraLookupEdit: TFrame},
  fMajDatas in 'fMajDatas.pas' {FrmMajDatas};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFrmMain, FrmMain);
  Application.CreateForm(TMDatas, MDatas);
  Application.Run;
end.
