unit fEditDatas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Grids, Vcl.DBGrids,
  JvExDBGrids, JvDBGrid, Vcl.StdCtrls, JvExStdCtrls, JvEdit;

type
  TFrmEditDatas = class(TForm)
    edt1: TJvEdit;
    Grid1: TJvDBGrid;
    ds1: TDataSource;
    lblProduct: TLabel;
    edtSearch: TJvEdit;
    lblSearch: TLabel;
    Label1: TLabel;
    procedure ds1DataChange(Sender: TObject; Field: TField);
    procedure edt1Change(Sender: TObject);
    procedure edt1Exit(Sender: TObject);
    procedure edt1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure edtSearchChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure lblProductClick(Sender: TObject);
  private
    FIdProduct: Integer;
    FIsDropdown: Boolean;
    procedure SetIdProduct(const Value: Integer);
    procedure FQryBeforeOpen(DataSet: TDataSet);
    procedure FQryAfterOpen(DataSet: TDataSet);
    procedure FQryAfterClose(DataSet: TDataSet);
    procedure ChangeDropdown(AIsVisible: Boolean);
  public
    property IdProduct: Integer read FIdProduct write SetIdProduct;
  end;

implementation

{$R *.dfm}

uses
  dmDatas,
  fSelDatas,
  FireDAC.Comp.Client,
  uMyTools;

procedure TFrmEditDatas.FormCreate(Sender: TObject);
var
  qry: TFDQuery;
begin
  edt1.Clear;
  FIdProduct := -1;
  lblSearch.Caption := string.Empty;
  ChangeDropdown(False);

  Grid1.Options := Grid1.Options - [TDBGridOption.dgEditing];

  qry := MDatas.GetQuery(ds1);
  qry.BeforeOpen := FQryBeforeOpen;
  qry.AfterOpen := FQryAfterOpen;
  qry.AfterClose := FQryAfterClose;
  ds1.DataSet := qry;

  qry.Open('SELECT ProductID, ProductName FROM Products ORDER BY ProductName;');
end;

procedure TFrmEditDatas.SetIdProduct(const Value: Integer);
begin
  FIdProduct := Value;
  if (FIdProduct > 0)
    and ((ds1.DataSet['ProductID'] <> Value)
         or (edt1.Text <> ds1.DataSet['ProductName']))
    and (ds1.DataSet.Locate('ProductID', Value, [])) then
    edt1.Text := ds1.DataSet['ProductName'];
end;

procedure TFrmEditDatas.ChangeDropdown(AIsVisible: Boolean);
begin
  FIsDropdown := AIsVisible;
  Grid1.Visible := FIsDropdown;
end;

procedure TFrmEditDatas.ds1DataChange(Sender: TObject; Field: TField);
begin
  if (FIdProduct > 0) then
  begin
    edt1.Text := ds1.DataSet['ProductName'];
    edt1.SelectAll;
    FIdProduct := ds1.DataSet['ProductID'];
  end;
end;

{$REGION 'TFDQuery'}
procedure TFrmEditDatas.FQryBeforeOpen(DataSet: TDataSet);
var
  lFld: TStringField;
begin
  if (not DataSet.FieldDefs.Updated) then
  begin
    DataSet.FieldDefs.Update;
    for var i: Integer := 0 to DataSet.FieldDefs.Count - 1 do
      DataSet.FieldDefs[i].CreateField(DataSet);
  end;

  lFld := TStringField.Create(DataSet);
  lFld.FieldKind := fkInternalCalc;
  lFld.FieldName := 'fld_search';
  lFld.Calculated := False;
  lFld.DataSet := DataSet;
end;

procedure TFrmEditDatas.FQryAfterOpen(DataSet: TDataSet);
var
  lFld: TField;
begin
  DataSet.FieldByName('ProductID').Visible := False;
  Grid1.Columns[0].Width := 300;

  lFld := DataSet.FieldByName('fld_search');
  lFld.Visible := False;

  while (not DataSet.Eof) do
  begin
    DataSet.Edit;
    lFld.AsString := Slugify(DataSet['ProductName']);
    DataSet.Post;
    DataSet.Next;
  end;
  DataSet.First;
end;

procedure TFrmEditDatas.FQryAfterClose(DataSet: TDataSet);
begin
  DataSet.FieldByName('fld_search').Free;
end;
{$ENDREGION}

procedure TFrmEditDatas.edt1Change(Sender: TObject);
begin
  if (string(edt1.Text).IsEmpty) then
    IdProduct := -1;
end;

procedure TFrmEditDatas.edt1Exit(Sender: TObject);
begin
  ChangeDropdown(False);
end;

procedure TFrmEditDatas.edt1KeyDown(Sender: TObject; var Key: Word; Shift:
    TShiftState);
begin 
    if (Shift = [ssAlt]) and (Key = VK_DOWN) then
    begin
      Grid1.Top := edt1.Top + edt1.Height;
      Grid1.Left := edt1.Left;
      ChangeDropdown(True);
    end
    else if (Key = VK_ESCAPE)
      or (Shift = [ssAlt]) and (Key = VK_UP) then
      ChangeDropdown(False)
    else if (FIsDropdown) then
    begin
      if (Key = VK_DOWN) then
        ds1.DataSet.Next
      else if (Key = VK_UP) then
        ds1.DataSet.Prior
      else if (Key = VK_RETURN) then
      begin
        FIdProduct := ds1.DataSet['ProductID'];
        edt1.Text := ds1.DataSet['ProductName'];
        ChangeDropdown(False);
      end;
    end;
end;

procedure TFrmEditDatas.edtSearchChange(Sender: TObject);
var
  str, txt: string;
begin
  str := edtSearch.Text;
  str := Slugify(str);
  if str = '' then
    lblSearch.Caption := 'Empty'
  else if ds1.DataSet.Locate('fld_search', str, [loPartialKey]) then
  begin
    txt := edtSearch.Text;
    lblSearch.Caption := ds1.DataSet['ProductName'];
    edtSearch.Text := ds1.DataSet['ProductName'];
    edtSearch.SelStart := txt.Length;
    edtSearch.SelLength := Length(edtSearch.Text) - edtSearch.SelStart;
  end
  else
    lblSearch.Caption := 'Not found';
end;

procedure TFrmEditDatas.lblProductClick(Sender: TObject);
var
  frm: TFrmSelDatas;
begin
  frm := TFrmSelDatas.Create(Self);
  try
    frm.TableName := 'Products';
    frm.KeyName := 'ProductID';
    frm.Id := IdProduct;

    if (frm.ShowModal = mrOk) then
      IdProduct := frm.Id;
  finally
    frm.Free;
  end;
end;

end.
