unit cShowGrid;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Grids;

type
  TFraShowGrid = class(TFrame)
    Edit1: TEdit;
    Grid1: TStringGrid;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
  private
  public
    constructor Create(AOwner: TComponent); override;
  end;

implementation

{$R *.dfm}

constructor TFraShowGrid.Create(AOwner: TComponent);
begin
  inherited;
  Edit1.Clear;
  Grid1.Visible := False;
  Height := Edit1.Top + Edit1.Height + 1;

  Grid1.Parent := TWinControl(AOwner);
  Grid1.Width := Edit1.Width;
end;

procedure TFraShowGrid.Button1Click(Sender: TObject);
begin
  Grid1.Visible := not Grid1.Visible;

  if (Grid1.Visible) then
  begin
    Grid1.Top := Top + Edit1.Top + Edit1.Height;
    Grid1.Left := Left + Edit1.Left;

    Grid1.BringToFront;
  end;
end;

end.
