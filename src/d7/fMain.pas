unit fMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TFrmMain = class(TForm)
    cbxTables: TComboBox;
    btnBaseList: TButton;
    btnListDatas: TButton;
    btnSelData: TButton;
    lblSelData: TLabel;
    btnAutoCompl: TButton;
    procedure btnAutoComplClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnBaseListClick(Sender: TObject);
    procedure btnListDatasClick(Sender: TObject);
    procedure btnSelDataClick(Sender: TObject);
  private
  public
  end;

var
  FrmMain: TFrmMain;

implementation

{$R *.dfm}

uses
  fBaseList,
  fLstDatas,
  fSelDatas,
  fTestAutoCompl;

procedure TFrmMain.FormCreate(Sender: TObject);
begin
  cbxTables.Clear;
  cbxTables.Items.Add('Employees');
  cbxTables.Items.Add('Categories');
  cbxTables.Items.Add('Customers');
  cbxTables.Items.Add('Shippers');
  cbxTables.Items.Add('Suppliers');
  cbxTables.Items.Add('Orders');
  cbxTables.Items.Add('Products');
  cbxTables.Items.Add('OrderDetails');
  cbxTables.Items.Add('Region');
  cbxTables.Items.Add('Territories');
  cbxTables.Items.Add('EmployeeTerritories');
  cbxTables.ItemIndex := 0;
  lblSelData.Caption := '';
end;

procedure TFrmMain.btnBaseListClick(Sender: TObject);  
var
  frm: TFrmBaseList;
begin
  if (cbxTables.ItemIndex < 0) then
    Exit;
  frm := TFrmBaseList.Create(Self);
  try
    frm.TableName := cbxTables.Text;
    frm.ShowModal;
  finally
    frm.Free;
  end;
end;

procedure TFrmMain.btnListDatasClick(Sender: TObject);  
var
  frm: TFrmLstDatas;
begin
  if (cbxTables.ItemIndex < 0) then
    Exit;
  frm := TFrmLstDatas.Create(Self);
  try
    frm.TableName := cbxTables.Text;
    frm.ShowModal;
  finally
    frm.Free;
  end;
end;

procedure TFrmMain.btnSelDataClick(Sender: TObject);
var
  frm: TFrmSelDatas;
  lid: string;
begin
  frm := TFrmSelDatas.Create(Self);
  try
    if (cbxTables.Text = 'Employees') then
        lid := 'EmployeeID'
    else if (cbxTables.Text = 'Categories') then
        lid := 'CategoryID'
    else if (cbxTables.Text = 'Customers') then
        lid := 'CustomerID'
    else if (cbxTables.Text = 'Shippers') then
        lid := 'ShipperID'
    else if (cbxTables.Text = 'Suppliers') then
        lid := 'SupplierID'
    else if (cbxTables.Text = 'Orders') then
        lid := 'OrderID'
    else if (cbxTables.Text = 'Products') then
        lid := 'ProductID'
    else if (cbxTables.Text = 'Region') then
        lid := 'RegionID'
    else if (cbxTables.Text = 'Territories') then
        lid := 'TerritoryID'
    else
        lid := '';
        
    frm.TableName := cbxTables.Text;
    frm.KeyName := lid;

    if (frm.ShowModal = mrOK) then
    begin
      lid := VarToStr(frm.Id);

      lblSelData.Caption := Format('%s: %s', [frm.TableName, lid])
    end
    else
      lblSelData.Caption := Format('%s: Cancel', [frm.TableName]);
  finally
    frm.Free;
  end;
end;               

procedure TFrmMain.btnAutoComplClick(Sender: TObject);
var
  frm: TFrmTestAutoCompl;
begin
  frm := TFrmTestAutoCompl.Create(Self);
  try
    frm.ShowModal;
  finally
    frm.Free;
  end;       
end;

end.
