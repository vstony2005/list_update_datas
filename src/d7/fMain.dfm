object FrmMain: TFrmMain
  Left = 735
  Top = 277
  Width = 713
  Height = 467
  Caption = 'FrmMain'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lblSelData: TLabel
    Left = 122
    Top = 149
    Width = 48
    Height = 13
    Caption = 'lblSelData'
  end
  object cbxTables: TComboBox
    Left = 32
    Top = 24
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 0
    Text = 'cbxTables'
  end
  object btnBaseList: TButton
    Left = 32
    Top = 64
    Width = 75
    Height = 25
    Caption = '&Base List'
    TabOrder = 1
    OnClick = btnBaseListClick
  end
  object btnListDatas: TButton
    Left = 32
    Top = 104
    Width = 75
    Height = 25
    Caption = '&Datas List'
    TabOrder = 2
    OnClick = btnListDatasClick
  end
  object btnSelData: TButton
    Left = 32
    Top = 144
    Width = 75
    Height = 25
    Caption = '&Select Data'
    TabOrder = 3
    OnClick = btnSelDataClick
  end
  object btnAutoCompl: TButton
    Left = 32
    Top = 200
    Width = 75
    Height = 25
    Caption = 'Auto Compl'
    TabOrder = 4
    OnClick = btnAutoComplClick
  end
end
