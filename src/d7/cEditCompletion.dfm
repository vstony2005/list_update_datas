object FraEditCompletion: TFraEditCompletion
  Left = 0
  Top = 0
  Width = 481
  Height = 359
  TabOrder = 0
  DesignSize = (
    481
    359)
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 32
    Height = 13
    Caption = 'Label1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsUnderline]
    ParentFont = False
    OnClick = Label1Click
  end
  object Edit1: TJvEdit
    Left = 80
    Top = 12
    Width = 377
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    OnChange = Edit1Change
    OnExit = Edit1Exit
    OnKeyDown = Edit1KeyDown
  end
  object GridDebug: TDBGrid
    Left = 160
    Top = 40
    Width = 320
    Height = 120
    DataSource = ds1
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object Grid1: TDBGrid
    Left = 40
    Top = 104
    Width = 320
    Height = 120
    DataSource = ds1
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object ds1: TDataSource
    OnDataChange = ds1DataChange
    Left = 88
    Top = 72
  end
end
