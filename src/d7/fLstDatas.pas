unit fLstDatas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZDataset, Spin, StdCtrls, JvExStdCtrls, JvEdit, ExtCtrls,
  DBCtrls, DB, Grids, DBGrids, JvExDBGrids, JvDBGrid, uParamCols;

type
  TMyDBGrid = class(TDBGrid);
  TMyQry = class(TZQuery);
  TFrmLstDatas = class(TForm)
    JvDBGrid1: TJvDBGrid;
    ds1: TDataSource;
    DBNavigator1: TDBNavigator;
    edtSearch: TJvEdit;
    lblNbLines: TLabel;
    btnMoins: TButton;
    btnPlus: TButton;
    btnGo: TButton;
    edtGoLine: TSpinEdit;
    lblPosition: TLabel;
    procedure btnGoClick(Sender: TObject);
    procedure btnMoinsClick(Sender: TObject);
    procedure btnPlusClick(Sender: TObject);
    procedure edtSearchChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure edtGoLineKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure JvDBGrid1AfterPaint(Sender: TObject);
    procedure JvDBGrid1GetBtnParams(Sender: TObject; Field: TField; AFont: TFont;
        var Background: TColor; var ASortMarker: TSortMarker; IsDown: Boolean);
    procedure JvDBGrid1TitleClick(Column: TColumn);
  private
    FIsAsc: Boolean;
    FTableCols: TTableCols;
    FQry: TZQuery;
    FTableName: string;
    procedure FQryAfterOpen(DataSet: TDataSet);
    procedure MoveBy(AValue: Integer);
    function IndexOfCol(const AColName: string): Integer;
    procedure SetTableName(const Value: string);
  public   
    property TableName: string read FTableName write SetTableName;
  end;

implementation

uses
  dmDatas;

{$R *.dfm}

{ TFrmLstDatas }

procedure TFrmLstDatas.FormCreate(Sender: TObject);
begin
  FTableCols := TTableCols.Create;
  JvDBGrid1.TitleButtons := True;
end;

procedure TFrmLstDatas.FormDestroy(Sender: TObject);
begin
  FTableCols.Free;
end;    

procedure TFrmLstDatas.btnGoClick(Sender: TObject);
begin
  if not Assigned(FQry) then Exit;

  if (edtGoLine.Value > 0) then
  begin
    FQry.First;
    MoveBy(edtGoLine.Value);
  end;
end;

procedure TFrmLstDatas.btnMoinsClick(Sender: TObject);
begin
  MoveBy(-TMyDBGrid(JvDBGrid1).VisibleRowCount);
end;

procedure TFrmLstDatas.btnPlusClick(Sender: TObject);
begin
  MoveBy(TMyDBGrid(JvDBGrid1).VisibleRowCount);
end;

procedure TFrmLstDatas.edtSearchChange(Sender: TObject);
begin
  if not Assigned(FQry) then Exit;

  FQry.Filtered := False;

  if (JvDBGrid1.SortedField <> '') and (edtSearch.Text <> '') then
  begin
    FQry.FilterOptions := [foCaseInsensitive,foNoPartialCompare];
    FQry.Filter := Format('%s LIKE %s',
                          [JvDBGrid1.SortedField,
                           QuotedStr('*' + edtSearch.Text + '*')]);
    FQry.Filtered := True;
  end;
end;

procedure TFrmLstDatas.FQryAfterOpen(DataSet: TDataSet);
begin
  FTableCols.PrepareDBGrid(JvDBGrid1.Columns);
end;

function TFrmLstDatas.IndexOfCol(const AColName: string): Integer; 
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to JvDBGrid1.Columns.Count - 1 do
  begin
    if (JvDBGrid1.Columns[i].FieldName = AColName) then
    begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrmLstDatas.MoveBy(AValue: Integer);
begin
  if not Assigned(FQry) then Exit;

  FQry.MoveBy(AValue);
end;

procedure TFrmLstDatas.SetTableName(const Value: string);
begin
  if (FTableName = Value) then Exit;

  FTableName := Value;

  FTableCols.TableName := FTableName;

  if Assigned(FQry) then
  begin
    FQry.Free;
    FQry := nil;
  end;

  FQry := MDatas.GetQuery(Self);
  FQry.SQL.Text := FTableCols.GetSql;

  FQry.AfterOpen := FQryAfterOpen;

  ds1.DataSet := FQry; // avant le open
  FQry.Open;
end;

procedure TFrmLstDatas.edtGoLineKeyDown(Sender: TObject; var Key: Word; Shift:
    TShiftState);
begin
  if (Key = VK_RETURN) then
    btnGoClick(btnGo);
end;

procedure TFrmLstDatas.JvDBGrid1AfterPaint(Sender: TObject);  
var
  nb, row, pos: Integer;
begin
  if not Assigned(FQry) then Exit;

  // nb lines visible in dbgrid
  nb := TMyDBGrid(JvDBGrid1).VisibleRowCount;
  // current line in dbgrid
  row := TMyDBGrid(JvDBGrid1).Row;
  lblNbLines.Caption := Format('Nb Lines: %d / Row: %d', [nb, row]);

  // current line in cds
  //pos := TMyQry(FQry).GetRecNo;  // TODO: � voir
  pos := 0;
  lblPosition.Caption := Format('Position: %d (%d-%d)',
                                [pos,
                                 pos-row+1,
                                 pos-row+nb]);
end;

procedure TFrmLstDatas.JvDBGrid1GetBtnParams(Sender: TObject; Field: TField;
    AFont: TFont; var Background: TColor; var ASortMarker: TSortMarker; IsDown:
    Boolean); 
const
  Direction: array[boolean] of TSortmarker = (smDown, smUp);
begin
  if (Field.FieldName = JvDBGrid1.SortedField) then
    ASortMarker := Direction[FIsAsc]
  else
    ASortMarker := smNone;
end;

procedure TFrmLstDatas.JvDBGrid1TitleClick(Column: TColumn);
var
  col, cola, cold: string;
begin
  if not Assigned(FQry) then Exit;

  col := Column.FieldName;
  cola := Format('%s Asc', [col]);
  cold := Format('%s Desc', [col]);

  if (FQry.IndexFieldNames = cola) then
  begin
    FQry.IndexFieldNames := cold;
    FIsAsc := False;
  end
  else
  begin
    FQry.IndexFieldNames := cola;
    FIsAsc := True;
  end;

  JvDBGrid1.SortedField := col;

  if (edtSearch.Text <> '') then
    edtSearchChange(edtSearch);
end;

end.
