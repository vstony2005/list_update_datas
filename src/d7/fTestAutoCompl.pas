unit fTestAutoCompl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cEditCompletion, StdCtrls;

type
  TFrmTestAutoCompl = class(TForm)
    frmAutoCompl: TFraEditCompletion;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    procedure IdChange(Sender: TObject);
  public
  end;

implementation

{$R *.dfm}

procedure TFrmTestAutoCompl.FormCreate(Sender: TObject);
begin
  frmAutoCompl.Init('Customers','CustomerID','CompanyName','CompanyName',
                    'CustomerID,CompanyName','CompanyName');  

  frmAutoCompl.OnIdChange := IdChange;
  frmAutoCompl.SetLabel('Customer');
end;

procedure TFrmTestAutoCompl.IdChange(Sender: TObject);
begin
  Label1.Caption := Format('ID: %s', [VarToStr(frmAutoCompl.Id)])
end;

end.
