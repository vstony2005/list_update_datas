unit dmDatas;

interface

uses
  SysUtils, Classes, DB, ZAbstractRODataset, ZAbstractDataset, ZDataset,
  ZAbstractConnection, ZConnection, MidasLib;

type
  TMDatas = class(TDataModule)
    ZConnection1: TZConnection;
    ZQuery1: TZQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
  public     
    function GetQuery(AOwner: TComponent = nil): TZQuery;
  end;

var
  MDatas: TMDatas;

implementation

{$R *.dfm}

uses
  Dialogs;

{ TMDatas }

procedure TMDatas.DataModuleCreate(Sender: TObject);
begin
  ZConnection1.Protocol := 'sqlite-3';
  ZConnection1.Database := '..\..\..\datas\instnwnd.db';
  ZConnection1.LoginPrompt := False;
  try
    try
      ZConnection1.Connect;
    except
      on e: exception do
        MessageDlg('Error: ' + e.Message, mtError, [mbOK], 0);
    end;
  finally
  end;
end;

function TMDatas.GetQuery(AOwner: TComponent): TZQuery;
var
  qry: TZQuery;
begin
  Result := nil;
  try
    qry := TZQuery.Create(AOwner);
    qry.Connection := ZConnection1;
    Result := qry;
  finally
  end;
end;

end.
