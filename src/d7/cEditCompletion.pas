unit cEditCompletion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, DB, Grids, DBGrids, StdCtrls, JvExStdCtrls, JvEdit;

type
  TFraEditCompletion = class(TFrame)
    Label1: TLabel;
    Edit1: TJvEdit;
    ds1: TDataSource;
    GridDebug: TDBGrid;
    Grid1: TDBGrid;
    procedure ds1DataChange(Sender: TObject; Field: TField);
    procedure Edit1Change(Sender: TObject);
    procedure Edit1Exit(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Label1Click(Sender: TObject);
  private
    FIsGridVisible: Boolean;
    FListFieldsSlug: TStringList;
    FIsFromEdit: Boolean;

    FFieldId: string;
    FFieldShow: string;
    FFieldsOrder: string;
    FFieldsSearch: Integer;
    FFieldsSelect: string;
    FId: variant;
    FOnIdChange: TNotifyEvent;
    FTableName: string;
    procedure SetFieldId(const Value: string);
    procedure SetTableName(const Value: string);

    procedure FQryAfterOpen(DataSet: TDataSet);

    function GetId: Variant;
    procedure SetFieldShow(const Value: string);
    procedure SetFieldsOrder(const Value: string);
    procedure SetFieldsSearch(const Value: string);
    procedure SetFieldsSelect(const Value: string);

    procedure SetGridVisible(AIsVisible: Boolean);
    procedure SetId(const Value: Variant);
    procedure ToogleGridVisible;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure Init(const ATableName,AFieldId,AFieldShow,AFieldsSearch: string;
          const AFieldsSelect: string = '';
          const AFieldsOrder: string = '');
          
    procedure SetLabel(const AValue: string);

    // (!!) mandatory
    property FieldId: string read FFieldId write SetFieldId;
    // (!!) mandatory
    property FieldShow: string read FFieldShow write SetFieldShow;
    property FieldsOrder: string read FFieldsOrder write SetFieldsOrder;
    // (!!) mandatory
    property FieldsSearch: string write SetFieldsSearch;
    property FieldsSelect: string read FFieldsSelect write SetFieldsSelect;
    property Id: Variant read GetId write SetId;
    // (!!) mandatory
    property TableName: string read FTableName write SetTableName;
  published
    property OnIdChange: TNotifyEvent read FOnIdChange write FOnIdChange;
  end;

implementation

{$R *.dfm}

uses
  dmDatas,
  fSelDatas,
  ZDataset;

constructor TFraEditCompletion.Create(AOwner: TComponent);
begin
  inherited;

  Edit1.Clear;
  Grid1.Visible := False;
  FListFieldsSlug := TStringList.Create;
  FListFieldsSlug.Delimiter := ';';

  Fid := Null;
  FIsFromEdit := False;

  Grid1.Parent := TWinControl(AOwner);
  Height := Grid1.Top + Grid1.Height + 2;
end;

destructor TFraEditCompletion.Destroy;
begin
  FListFieldsSlug.Free;
  inherited;
end;

procedure TFraEditCompletion.Init(const ATableName,AFieldId,AFieldShow,AFieldsSearch: string;
      const AFieldsSelect: string = '';
      const AFieldsOrder: string = '');
begin
  FieldId := AFieldId;
  FieldShow := AFieldShow;
  FieldsSearch := AFieldsSearch;
  FieldsSelect := AFieldsSelect;
  FieldsOrder := AFieldsOrder;
  TableName := ATableName;
end;

procedure TFraEditCompletion.FQryAfterOpen(DataSet: TDataSet);

  function FieldsToText: string;
  var
    str: string;
    i: Integer;
  begin
    Result := '';

    for i := 0 to FListFieldsSlug.Count - 1 do
    begin
      str := FListFieldsSlug[i];
      if (Result = '') then
        Result := Dataset[str]
      else
        Result := Format('%s %s', [Result, Dataset[str]]);
    end;
  end;

var
  lFld: TField;
begin
  lFld := DataSet.FieldByName('fld_search');
  lFld.Visible := False;
  if (FieldId <> '') then
    DataSet.FieldByName(FieldId).Visible := False;

  Grid1.Columns[0].Width := 300;

  while (not DataSet.Eof) do
  begin
    DataSet.Edit;
    // TODO: � revoir
    lFld.AsString := LowerCase(FieldsToText); //Slugify(FieldsToText);
    DataSet.Post;
    DataSet.Next;
  end;
  DataSet.First;
end;

procedure TFraEditCompletion.ds1DataChange(Sender: TObject; Field: TField);
begin
  if not VarIsNull(FId) and (not FIsFromEdit) then
  begin
    FIsFromEdit := True;
    Edit1.Text := ds1.DataSet[FieldShow];    
    Id := ds1.DataSet[FieldId];
    Edit1.SelectAll;    
    FIsFromEdit := False;
  end;
end;

procedure TFraEditCompletion.Edit1Change(Sender: TObject);
var
  str, txt: string;
  isChange: Boolean;
begin
  if not Assigned(ds1.DataSet) or (ds1.DataSet.State = dsInactive) then
    Exit;

  FIsFromEdit := True;

  str := Edit1.Text;
  // TODO: � revoir
//  str := '';
  str := LowerCase(str);
  if (str <> '') and ds1.DataSet.Locate('fld_search', str, [loPartialKey]) then
  begin
    txt := Edit1.Text;
    isChange := FId <> ds1.DataSet[FieldId];
    if (isChange) then
      FId := ds1.DataSet[FieldId];
    Edit1.Text := ds1.DataSet[FieldShow];

    if (isChange) and Assigned(FOnIdChange) then
        FOnIdChange(Self);

    Edit1.SelStart := Length(txt);
    Edit1.SelLength := Length(Edit1.Text) - Edit1.SelStart;
  end
  else
    Id := Null;

  FIsFromEdit := False;
end;

procedure TFraEditCompletion.Edit1Exit(Sender: TObject);
begin
  SetGridVisible(False);
end;

procedure TFraEditCompletion.Edit1KeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
begin
  if (Shift = [ssAlt]) and (Key = VK_DOWN) then
  begin
    if (not FIsGridVisible) then
    begin
      Grid1.Top := Edit1.Top + Edit1.Height;
      Grid1.Left := Edit1.Left;
      SetGridVisible(True);
    end;
  end
  else if (Key = VK_ESCAPE)
    or (Shift = [ssAlt]) and (Key = VK_UP) then
    SetGridVisible(False)
  else if (FIsGridVisible) then
  begin
    if (Key = VK_DOWN) then
      ds1.DataSet.Next
    else if (Key = VK_UP) then
      ds1.DataSet.Prior
    else if (Key = VK_RETURN) then
    begin
      Id := ds1.DataSet[FieldId];
      SetGridVisible(False);
    end;
  end;
end;

procedure TFraEditCompletion.SetFieldId(const Value: string);
begin
  FFieldId := Value;
end;

procedure TFraEditCompletion.SetFieldShow(const Value: string);
begin
  FFieldShow := Value;
end;

procedure TFraEditCompletion.SetFieldsOrder(const Value: string);
begin
  FFieldsOrder := Value;
end;

procedure TFraEditCompletion.SetFieldsSearch(const Value: String);
begin
  FListFieldsSlug.DelimitedText := Value;
end;

procedure TFraEditCompletion.SetFieldsSelect(const Value: string);
begin
  FFieldsSelect := Value;
end;

procedure TFraEditCompletion.SetTableName(const Value: string);
var
  qry: TZQuery;
begin
  FTableName := Value;

  if Assigned(ds1.DataSet) and (ds1.DataSet.Active) then
  begin
    ds1.DataSet.Close;
    ds1.Free;
    ds1 := nil;
  end;

  qry := MDatas.GetQuery(ds1);
  qry.AfterOpen := FQryAfterOpen;
  ds1.DataSet := qry;

  if (FieldsSelect = '') then
    qry.SQL.Add('SELECT *')
  else
    qry.SQL.Add(Format('SELECT %s', [FieldsSelect]));

  qry.SQL.Add(' , NULL fld_search ');
  qry.SQL.Add(Format('FROM %s', [TableName]));

  if (FieldsOrder = '') then
    qry.SQL.Add(Format('ORDER BY %s', [FieldsOrder]));

  qry.Open;
end;

function TFraEditCompletion.GetId: Variant;
begin
  if not VarIsNull(FId)
    and Assigned(ds1.DataSet)
    and (ds1.DataSet.State <> dsInactive)
    and (Edit1.Text = ds1.DataSet[FieldShow]) then
    Result := FId
  else
    Result := Null;
end;

procedure TFraEditCompletion.Label1Click(Sender: TObject);
var
  frm: TFrmSelDatas;
begin
  frm := TFrmSelDatas.Create(Self);
  try
    frm.TableName := TableName;
    frm.KeyName := FieldId;
    frm.Id := Id;

    SetGridVisible(False);

    if (frm.ShowModal = mrOk) then
      Id := frm.Id;
  finally
    frm.Free;
  end;
end;

procedure TFraEditCompletion.SetId(const Value: Variant);
begin
  if (Value <> FId) then
  begin
    FId := Value;

    if (not FIsFromEdit) then
    begin
      if ds1.DataSet.Locate(FieldId, FId, []) then
        Edit1.Text := ds1.DataSet[FieldShow];
    end;

    if Assigned(FOnIdChange) then
      FOnIdChange(Self);
  end;
end;

procedure TFraEditCompletion.SetGridVisible(AIsVisible: Boolean);
begin
  if (FIsGridVisible <> AIsVisible) then
    ToogleGridVisible;
end;

procedure TFraEditCompletion.ToogleGridVisible;
begin
  FIsGridVisible := not FIsGridVisible;
  Grid1.Visible := FIsGridVisible;

  if (FIsGridVisible) then
  begin
    Grid1.Top := Top + Edit1.Top + Edit1.Height;
    Grid1.Left := Left + Edit1.Left;

    Grid1.BringToFront;
  end;
end;

procedure TFraEditCompletion.SetLabel(const AValue: string);
begin
  Label1.Caption := AValue;
end;

end.
