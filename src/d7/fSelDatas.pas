unit fSelDatas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fBaseList, DB, StdCtrls, JvExStdCtrls, JvEdit, Grids, DBGrids,
  JvExDBGrids, JvDBGrid, Buttons;

type
  TFrmSelDatas = class(TFrmBaseList)
    btnOk: TSpeedButton;
    btnCancel: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
  private
    FId: Variant;
    FKeyName: string;
    function GetId: Variant;
    procedure SetId(const Value: Variant);
  public       
    property Id: Variant read GetId write SetId;
    property KeyName: string read FKeyName write FKeyName;
  end;

implementation

{$R *.dfm}

{ TFrmSelDatas }        

procedure TFrmSelDatas.FormCreate(Sender: TObject);
begin
  inherited;   
  FId := -1;
  FKeyName := 'id';
end;

procedure TFrmSelDatas.btnCancelClick(Sender: TObject);
begin
  inherited;   
  ModalResult := mrCancel;
end;

procedure TFrmSelDatas.btnOkClick(Sender: TObject);
begin
  inherited;    
  ModalResult := mrOk;
end;

function TFrmSelDatas.GetId: Variant;
begin
  Result := Null;
  if not Assigned(FQry) or (KeyName = '') then
    Exit;

  Result := FQry[KeyName];
end;

procedure TFrmSelDatas.SetId(const Value: Variant);
begin
  if not Assigned(FQry) or (KeyName = '') then
    Exit;

  FQry.Locate(KeyName, Value, []);
end;

end.
