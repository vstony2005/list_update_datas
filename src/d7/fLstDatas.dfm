object FrmLstDatas: TFrmLstDatas
  Left = 830
  Top = 281
  Width = 679
  Height = 436
  Caption = 'Datas List'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    663
    397)
  PixelsPerInch = 96
  TextHeight = 13
  object lblNbLines: TLabel
    Left = 80
    Top = 336
    Width = 49
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'lblNbLines'
  end
  object lblPosition: TLabel
    Left = 80
    Top = 366
    Width = 47
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'lblPosition'
  end
  object JvDBGrid1: TJvDBGrid
    Left = 16
    Top = 16
    Width = 625
    Height = 281
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = ds1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnTitleClick = JvDBGrid1TitleClick
    OnGetBtnParams = JvDBGrid1GetBtnParams
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 17
    TitleRowHeight = 17
    OnAfterPaint = JvDBGrid1AfterPaint
  end
  object DBNavigator1: TDBNavigator
    Left = 400
    Top = 304
    Width = 240
    Height = 25
    DataSource = ds1
    Anchors = [akRight, akBottom]
    TabOrder = 1
  end
  object edtSearch: TJvEdit
    Left = 16
    Top = 306
    Width = 361
    Height = 21
    Anchors = [akLeft, akRight, akBottom]
    TabOrder = 2
    OnChange = edtSearchChange
    TextHint = 'Search'
  end
  object btnMoins: TButton
    Left = 16
    Top = 360
    Width = 49
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = '<<'
    TabOrder = 3
    OnClick = btnMoinsClick
  end
  object btnPlus: TButton
    Left = 200
    Top = 360
    Width = 49
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = '>>'
    TabOrder = 4
    OnClick = btnPlusClick
  end
  object btnGo: TButton
    Left = 568
    Top = 360
    Width = 49
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Go'
    TabOrder = 5
    OnClick = btnGoClick
  end
  object edtGoLine: TSpinEdit
    Left = 440
    Top = 361
    Width = 121
    Height = 22
    Anchors = [akRight, akBottom]
    MaxValue = 0
    MinValue = 0
    TabOrder = 6
    Value = 0
    OnKeyDown = edtGoLineKeyDown
  end
  object ds1: TDataSource
    Left = 328
    Top = 208
  end
end
