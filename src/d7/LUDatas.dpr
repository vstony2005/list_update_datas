program LUDatas;

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Forms,
  fMain in 'fMain.pas' {FrmMain},
  uParamCols in 'uParamCols.pas',
  uMyMessages in 'uMyMessages.pas',
  dmDatas in 'dmDatas.pas' {MDatas: TDataModule},
  fBaseList in 'fBaseList.pas' {FrmBaseList},
  fLstDatas in 'fLstDatas.pas' {FrmLstDatas},
  fSelDatas in 'fSelDatas.pas' {FrmSelDatas},
  fTestAutoCompl in 'fTestAutoCompl.pas' {FrmTestAutoCompl},
  cEditCompletion in 'cEditCompletion.pas' {FraEditCompletion: TFrame};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TMDatas, MDatas);
  Application.CreateForm(TFrmMain, FrmMain);
  Application.Run;
end.
