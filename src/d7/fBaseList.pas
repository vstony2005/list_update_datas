unit fBaseList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, DB, JvExStdCtrls, JvEdit, Grids, DBGrids,
  JvExDBGrids, JvDBGrid, uParamCols, ZDataset;

type
  TMyGrid = class(TDBGrid);
  TMyQry = class(TZQuery);
  TFrmBaseList = class(TForm)
    Grid1: TJvDBGrid;
    edtSearch: TJvEdit;
    ds1: TDataSource;
    btnFirst: TSpeedButton;
    btnPrevious: TSpeedButton;
    btnNext: TSpeedButton;
    btnLast: TSpeedButton;
    lblPosition: TLabel;
    procedure btnFirstClick(Sender: TObject);
    procedure btnLastClick(Sender: TObject);
    procedure btnNextClick(Sender: TObject);
    procedure btnPreviousClick(Sender: TObject);
    procedure edtSearchChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Grid1AfterPaint(Sender: TObject);
    procedure Grid1GetBtnParams(Sender: TObject; Field: TField; AFont: TFont; var
        Background: TColor; var ASortMarker: TSortMarker; IsDown: Boolean);
    procedure Grid1TitleClick(Column: TColumn);
  private            
    FIsAsc: Boolean;
    FTableName: string;
    FTableCols: TTableCols;
    procedure SetTableName(const Value: string);
    procedure FQryAfterOpen(DataSet: TDataSet);
    procedure MoveBy(AValue: Integer);
  protected
    FQry: TZQuery;
  public  
    property TableName: string read FTableName write SetTableName;
  end;

implementation

uses dmDatas;

{$R *.dfm}    

procedure TFrmBaseList.FormCreate(Sender: TObject);
begin
  FTableCols := TTableCols.Create;
  Grid1.TitleButtons := True;
  Grid1.Options := Grid1.Options - [dgEditing];
  Grid1.Options := Grid1.Options + [dgRowSelect];
  lblPosition.Caption := '';
end;

procedure TFrmBaseList.FormDestroy(Sender: TObject);
begin
  FTableCols.Free;
end;

procedure TFrmBaseList.btnFirstClick(Sender: TObject);
begin
  FQry.First;
end;

procedure TFrmBaseList.btnLastClick(Sender: TObject);
begin
  FQry.Last;
end;

procedure TFrmBaseList.btnNextClick(Sender: TObject);
begin
  MoveBy(TMyGrid(Grid1).VisibleRowCount);
end;

procedure TFrmBaseList.btnPreviousClick(Sender: TObject);
begin
  MoveBy(-TMyGrid(Grid1).VisibleRowCount);
end;

procedure TFrmBaseList.edtSearchChange(Sender: TObject);
begin        
  if not Assigned(FQry) then Exit;

  FQry.Filtered := False;

  if (Grid1.SortedField <> '') and (edtSearch.Text <> '') then
  begin
    FQry.FilterOptions := [foCaseInsensitive,foNoPartialCompare];
    FQry.Filter := Format('%s LIKE %s',
                          [Grid1.SortedField,
                           QuotedStr('*' + edtSearch.Text + '*')]);
    FQry.Filtered := True;
  end;
end;

procedure TFrmBaseList.Grid1AfterPaint(Sender: TObject);  
var
  nb, row, pos, recs: Integer;
begin
  if not Assigned(FQry) then Exit;

  nb := TMyGrid(Grid1).VisibleRowCount;
  row := TMyGrid(Grid1).Row;
  pos := TMyQry(FQry).GetRecNo;
  recs := FQry.RecordCount;

  lblPosition.Caption := Format('%d-%d on %d',
                                [pos-row+1,
                                 pos-row+nb+1,
                                 recs]);
end;

procedure TFrmBaseList.Grid1GetBtnParams(Sender: TObject; Field: TField; AFont:
    TFont; var Background: TColor; var ASortMarker: TSortMarker; IsDown:
    Boolean);
const
  Direction: array[boolean] of TSortmarker = (smDown, smUp);
begin
  if (Field.FieldName = Grid1.SortedField) then
    ASortMarker := Direction[FIsAsc]
  else
    ASortMarker := smNone;
end;

procedure TFrmBaseList.Grid1TitleClick(Column: TColumn);       
var
  col, cola, cold: string;
begin
  if not Assigned(FQry) then Exit;

  col := Column.FieldName;
  cold := Format('%s Desc', [col]);
  cola := Format('%s Asc', [col]);

  if FQry.IndexFieldNames = cola then
  begin
    FQry.IndexFieldNames := cold;
    FIsAsc := False;
  end
  else
  begin
    FQry.IndexFieldNames := cola;
    FIsAsc := True;
  end;

  Grid1.SortedField := col;

  if (edtSearch.Text <> '') then
    edtSearchChange(edtSearch);
end;

procedure TFrmBaseList.FQryAfterOpen(DataSet: TDataSet);
begin
  FTableCols.PrepareDBGrid(Grid1.Columns);
end;

procedure TFrmBaseList.MoveBy(AValue: Integer);
begin
  if not Assigned(FQry) then Exit;

  FQry.MoveBy(AValue);
end;

procedure TFrmBaseList.SetTableName(const Value: string);
begin
  if (FTableName = Value) then Exit;

  FTableName := Value;

  FTableCols.TableName := FTableName;

  if Assigned(FQry) then
  begin
    FQry.Free;
    FQry := nil;
  end;

  FQry := MDatas.GetQuery(Self);
  FQry.SQL.Text := FTableCols.GetSql;

  FQry.AfterOpen := FQryAfterOpen;

  ds1.DataSet := FQry; // avant le open
  FQry.Open;
end;

end.
