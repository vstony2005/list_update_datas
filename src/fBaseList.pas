unit fBaseList;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  Vcl.Forms, System.Classes, Vcl.Graphics,
  Data.DB, Vcl.StdCtrls, Vcl.Controls, Vcl.Buttons, JvExStdCtrls, JvEdit,
  Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid, FireDAC.Comp.Client,
  uParamCols;

type
  TMyGrid = class(TDBGrid);
  TMyQry = class(TFDQuery);

  TFrmBaseList = class(TForm)
    Grid1: TJvDBGrid;
    edtSearch: TJvEdit;
    ds1: TDataSource;
    btnFirst: TSpeedButton;
    btnPrevious: TSpeedButton;
    btnNext: TSpeedButton;
    btnLast: TSpeedButton;
    lblPosition: TLabel;
    procedure btnFirstClick(Sender: TObject);
    procedure btnLastClick(Sender: TObject);
    procedure btnNextClick(Sender: TObject);
    procedure btnPreviousClick(Sender: TObject);
    procedure edtSearchChange(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Grid1AfterPaint(Sender: TObject);
    procedure Grid1GetBtnParams(Sender: TObject; Field: TField; AFont: TFont; var
        Background: TColor; var ASortMarker: TSortMarker; IsDown: Boolean);
    procedure Grid1TitleClick(Column: TColumn);
  private
    FIsAsc: Boolean;
    FTableName: string;
    FTableCols: TTableCols;
    procedure SetTableName(const Value: string);
    procedure FQryAfterOpen(DataSet: TDataSet);
    procedure MoveBy(AValue: Integer);
  protected
    FQry: TFDQuery;
  public
    property TableName: string read FTableName write SetTableName;
  end;

implementation

{$R *.dfm}

uses
  dmDatas;

procedure TFrmBaseList.FormCreate(Sender: TObject);
begin
  FTableCols := TTableCols.Create;
  Grid1.TitleButtons := True;
  Grid1.Options := Grid1.Options - [TDBGridOption.dgEditing];
  Grid1.Options := Grid1.Options + [TDBGridOption.dgRowSelect];
  lblPosition.Caption := string.Empty;
end;

procedure TFrmBaseList.FormDestroy(Sender: TObject);
begin
  FTableCols.Free;
end;

procedure TFrmBaseList.SetTableName(const Value: string);
begin
  if (FTableName = Value) then Exit;

  FTableName := Value;

  FTableCols.TableName := FTableName;

  if Assigned(FQry) then
  begin
    FQry.Free;
    FQry := nil;
  end;

  FQry := MDatas.GetQuery(Self);
  FQry.SQL.Text := FTableCols.GetSql;

  FQry.AfterOpen := FQryAfterOpen;

  ds1.DataSet := FQry; // avant le open
  FQry.Open;
end;

procedure TFrmBaseList.FQryAfterOpen(DataSet: TDataSet);
begin
  FTableCols.PrepareDBGrid(Grid1.Columns);
end;

procedure TFrmBaseList.edtSearchChange(Sender: TObject);
begin
  if not Assigned(FQry) then Exit;

  FQry.Filtered := False;

  if (Grid1.SortedField <> string.Empty)
    and (edtSearch.Text <> '') then
  begin
    FQry.FilterOptions := [foCaseInsensitive,foNoPartialCompare];
    FQry.Filter := Format('%s LIKE ''%%%s%%''',
                          [Grid1.SortedField,
                           edtSearch.Text]);
    FQry.Filtered := True;
  end;
end;

{$REGION 'Buttons'}
procedure TFrmBaseList.btnFirstClick(Sender: TObject);
begin
  FQry.First;
end;

procedure TFrmBaseList.btnPreviousClick(Sender: TObject);
begin
  MoveBy(-TMyGrid(Grid1).VisibleRowCount);
end;

procedure TFrmBaseList.btnNextClick(Sender: TObject);
begin
  MoveBy(TMyGrid(Grid1).VisibleRowCount);
end;

procedure TFrmBaseList.btnLastClick(Sender: TObject);
begin
  FQry.Last;
end;
{$ENDREGION}

{$REGION 'Grid'}
procedure TFrmBaseList.Grid1TitleClick(Column: TColumn);
var
  col, cold: string;
begin
  if not Assigned(FQry) then Exit;

  col := Column.FieldName;
  cold := Format('%s:D', [col]);

  if FQry.IndexFieldNames = col then
  begin
    FQry.IndexFieldNames := cold;
    FIsAsc := False;
  end
  else
  begin
    FQry.IndexFieldNames := col;
    FIsAsc := True;
  end;

  Grid1.SortedField := col;

  if (edtSearch.Text <> string.Empty) then
    edtSearchChange(edtSearch);
end;

procedure TFrmBaseList.Grid1GetBtnParams(Sender: TObject; Field: TField; AFont:
    TFont; var Background: TColor; var ASortMarker: TSortMarker; IsDown:
    Boolean);
const
  Direction: array[boolean] of TSortmarker = (smDown, smUp);
begin
  if (Field.FieldName = Grid1.SortedField) then
    ASortMarker := Direction[FIsAsc]
  else
    ASortMarker := smNone;
end;

procedure TFrmBaseList.Grid1AfterPaint(Sender: TObject);
var
  nb, row, pos, recs: Integer;
begin
  if not Assigned(FQry) then Exit;

  nb := TMyGrid(Grid1).VisibleRowCount;
  row := TMyGrid(Grid1).Row;
  pos := TMyQry(FQry).GetRecNo;
  recs := FQry.RecordCount;

  lblPosition.Caption := Format('%d-%d on %d',
                                [pos-row+1,
                                 pos-row+nb+1,
                                 recs]);
end;
{$ENDREGION}

procedure TFrmBaseList.MoveBy(AValue: Integer);
begin
  if not Assigned(FQry) then Exit;

  FQry.MoveBy(AValue);
end;

end.
