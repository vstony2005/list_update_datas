unit dmDatas;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.SQLite,
  FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs,
  FireDAC.Phys.SQLiteWrapper.Stat, FireDAC.VCLUI.Wait, Data.DB,
  FireDAC.Comp.Client, Firedac.DApt;

type
  TMDatas = class(TDataModule)
    FDConnection1: TFDConnection;
    procedure DataModuleCreate(Sender: TObject);
  private
  public
    function GetQuery(AOwner: TComponent = nil): TFDQuery;
  end;

var
  MDatas: TMDatas;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

uses
  Vcl.Dialogs, MidasLib;

procedure TMDatas.DataModuleCreate(Sender: TObject);
begin
  FDConnection1.Params.Database := '..\..\..\datas\instnwnd.db';
  FDConnection1.LoginPrompt := False;
  try
    try
      FDConnection1.Open;
    except
      on e: exception do
        MessageDlg('Error: ' + e.Message, TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], 0);
    end;
  finally
  end;
end;

function TMDatas.GetQuery(AOwner: TComponent = nil): TFDQuery;
var
  qry: TFDQuery;
begin
  Result := nil;
  qry := TFDQuery.Create(AOwner);
  qry.Connection := FDConnection1;
  Result := qry;
end;

end.
