unit uMyTools;

interface

function UnAccented(const AString: string): string;
function Slugify(const AString: string): string;

implementation

uses
  System.SysUtils,
  System.RegularExpressionsCore;

function UnAccented(const AString: string): string;

  (*
  function RemoveDiacritics(const aValue: string): string;
  begin
    SetLength(Result, FoldString(MAP_COMPOSITE, PChar(aValue), Length(aValue), nil, 0));
    FoldString(MAP_COMPOSITE, PChar(aValue), Length(aValue), PChar(Result), Length(Result));

    for var i := Result.Length downto 1 do
      if Result[i].GetUnicodeCategory = TUnicodeCategory.ucNonSpacingMark then
        Delete(Result, i, 1);
  end;

  //uses System.Character
  function SansAccent(WCh : WideChar): WideChar;
  const
    Char_Accents      : PWideChar = '�����������������������������������������������������';
    Char_Sans_Accents : PWideChar = 'AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn';
  asm
          CMP     AX, 128
          JB      @@2

          MOV     ECX, 53
          PUSH    EDI
          MOV     EDI, Char_Accents
          REPNE   SCASW      // Recherche le caract�re
          JNE     @@1        // Pas trouv�
          SUB     EDI, Char_Accents
          ADD     EDI, Char_Sans_Accents
          MOV     AX, [EDI - 2]
  @@1:   POP     EDI
  @@2:
  end;
    *)

var
  K: TArray<Byte>;
begin
  K := TEncoding.Convert(TEncoding.Unicode,
                         TEncoding.ASCII,
                         TEncoding.Unicode.GetBytes(AString));
  Result := StringOf(K);
end;

function Slugify(const AString: string): string;
var
  str: string;
  reg: TPerlRegEx;
begin
  Result := string.Empty;

  if (Trim(AString) = string.Empty) then
    Exit;

  str := UnAccented(AString);
  str := str.Trim.ToLower;

  reg := TPerlRegEx.Create;
  try
    reg.Subject := str;

    reg.RegEx := '[^a-z0-9]';
    reg.Replacement := ' ';
    reg.ReplaceAll;

    reg.RegEx := '\s{2,}';
    reg.Replacement := ' ';
    reg.ReplaceAll;

    reg.Subject := reg.Subject.Trim;

    reg.RegEx := '\s';
    reg.Replacement := '_';
    reg.ReplaceAll;

    Result := reg.Subject;
  finally
    reg.Free;
  end;
end;

end.
