unit fMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, cShowGrid;

type
  TFrmMain = class(TForm)
    btnBaseList: TButton;
    cbxTables: TComboBox;
    btnListDatas: TButton;
    btnSelDatas: TButton;
    ListBox1: TListBox;
    btnEditDatas: TButton;
    FraShowGrid1: TFraShowGrid;
    Memo1: TMemo;
    btnAutoCompletion: TButton;
    btnUpdDatas: TButton;
    procedure btnAutoCompletionClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnBaseListClick(Sender: TObject);
    procedure btnEditDatasClick(Sender: TObject);
    procedure btnListDatasClick(Sender: TObject);
    procedure btnSelDatasClick(Sender: TObject);
    procedure btnUpdDatasClick(Sender: TObject);
  private
  public
  end;

var
  FrmMain: TFrmMain;

implementation

{$R *.dfm}

uses
  fBaseList,
  fLstDatas,
  fSelDatas,
  fEditDatas,
  fMajDatas,
  fTestAutoCompletion;

procedure TFrmMain.FormCreate(Sender: TObject);
begin
  cbxTables.Clear;
  cbxTables.Items.Add('Employees');
  cbxTables.Items.Add('Categories');
  cbxTables.Items.Add('Customers');
  cbxTables.Items.Add('Shippers');
  cbxTables.Items.Add('Suppliers');
  cbxTables.Items.Add('Orders');
  cbxTables.Items.Add('Products');
  cbxTables.Items.Add('OrderDetails');
  cbxTables.Items.Add('Region');
  cbxTables.Items.Add('Territories');
  cbxTables.Items.Add('EmployeeTerritories');
  cbxTables.ItemIndex := 0;
end;

procedure TFrmMain.btnBaseListClick(Sender: TObject);
var
  frm: TFrmBaseList;
begin
  if (cbxTables.ItemIndex < 0) then
    Exit;
  frm := TFrmBaseList.Create(Self);
  try
    frm.TableName := cbxTables.Text;
    frm.ShowModal;
  finally
    frm.Free;
  end;
end;

procedure TFrmMain.btnEditDatasClick(Sender: TObject);
var
  frm: TFrmEditDatas;
begin
  frm := TFrmEditDatas.Create(Self);
  try
    frm.ShowModal;
  finally
    frm.Free
  end;
end;

procedure TFrmMain.btnListDatasClick(Sender: TObject);
var
  frm: TFrmLstDatas;
begin
  if (cbxTables.ItemIndex < 0) then
    Exit;
  frm := TFrmLstDatas.Create(Self);
  try
    frm.TableName := cbxTables.Text;
    frm.ShowModal;
  finally
    frm.Free;
  end;
end;

procedure TFrmMain.btnSelDatasClick(Sender: TObject);
var
  frm: TFrmSelDatas;
  res: Variant;
begin
  if (cbxTables.ItemIndex < 0) then
    Exit;
  frm := TFrmSelDatas.Create(Self);
  try
    frm.TableName := cbxTables.Text;


    if (cbxTables.Text = 'Employees') then
      frm.KeyName := 'EmployeeID'
    else if (cbxTables.Text = 'Categories') then
      frm.KeyName := 'CategoryID'
    else if (cbxTables.Text = 'Customers') then
      frm.KeyName := 'CustomerID'
    else if (cbxTables.Text = 'Shippers') then
      frm.KeyName := 'ShipperID'
    else if (cbxTables.Text = 'Suppliers') then
      frm.KeyName := 'SupplierID'
    else if (cbxTables.Text = 'Orders') then
      frm.KeyName := 'OrderID'
    else if (cbxTables.Text = 'Products') then
      frm.KeyName := 'ProductID'
    else if (cbxTables.Text = 'Region') then
      frm.KeyName := 'RegionID'
    else if (cbxTables.Text = 'Territories') then
      frm.KeyName := 'TerritoryID';

    ListBox1.Items.Add(frm.TableName);
    if (frm.ShowModal = mrOk) then
    begin
      res := frm.Id;
      ListBox1.Items.Add(Format('id: %s', [VarToStr(res)]));
    end
    else
      ListBox1.Items.Add('Cancel');
  finally
    frm.Free;
  end;
end;

procedure TFrmMain.btnAutoCompletionClick(Sender: TObject);
var
  frm: TFrmTestAutoCompl;
begin
  frm := TFrmTestAutoCompl.Create(Self);
  try
    frm.ShowModal;
  finally
    frm.Free;
  end;
end;

procedure TFrmMain.btnUpdDatasClick(Sender: TObject);
var
  frm: TFrmMajDatas;
begin
  frm := TFrmMajDatas.Create(Self);
  try
    frm.ShowModal;
  finally
    frm.Free;
  end;
end;

end.
