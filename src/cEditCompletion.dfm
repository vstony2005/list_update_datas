object FraEditCompletion: TFraEditCompletion
  Left = 0
  Top = 0
  Width = 464
  Height = 480
  TabOrder = 0
  DesignSize = (
    464
    480)
  object Label1: TLabel
    Left = 9
    Top = 20
    Width = 34
    Height = 15
    Caption = 'Label1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = [fsUnderline]
    ParentFont = False
    OnClick = Label1Click
  end
  object Edit1: TButtonedEdit
    Left = 73
    Top = 17
    Width = 368
    Height = 23
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    TextHint = 'Search'
    OnChange = Edit1Change
    OnExit = Edit1Exit
    OnKeyDown = Edit1KeyDown
  end
  object GridDebug: TDBGrid
    Left = 135
    Top = 56
    Width = 320
    Height = 120
    DataSource = ds1
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
  end
  object Grid1: TDBGrid
    Left = 72
    Top = 184
    Width = 320
    Height = 120
    DataSource = ds1
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
  end
  object ds1: TDataSource
    OnDataChange = ds1DataChange
    Left = 48
    Top = 88
  end
end
