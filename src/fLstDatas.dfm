object FrmLstDatas: TFrmLstDatas
  Left = 0
  Top = 0
  Caption = 'FrmLstDatas'
  ClientHeight = 441
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyPress = FormKeyPress
  DesignSize = (
    624
    441)
  TextHeight = 15
  object lblNbLines: TLabel
    Left = 8
    Top = 418
    Width = 60
    Height = 15
    Anchors = [akLeft, akBottom]
    Caption = 'Nb Lines : -'
  end
  object lblPosition: TLabel
    Left = 8
    Top = 397
    Width = 54
    Height = 15
    Anchors = [akLeft, akBottom]
    Caption = 'Position: -'
  end
  object DBNavigator1: TDBNavigator
    Left = 333
    Top = 372
    Width = 240
    Height = 25
    DataSource = ds1
    Anchors = [akRight, akBottom]
    TabOrder = 0
  end
  object JvDBGrid1: TJvDBGrid
    Left = 8
    Top = 8
    Width = 565
    Height = 329
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = ds1
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    OnTitleClick = JvDBGrid1TitleClick
    OnGetBtnParams = JvDBGrid1GetBtnParams
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 19
    TitleRowHeight = 19
    OnAfterPaint = JvDBGrid1AfterPaint
  end
  object edtSearch: TJvEdit
    Left = 8
    Top = 343
    Width = 565
    Height = 23
    Anchors = [akLeft, akRight, akBottom]
    TabOrder = 2
    Text = ''
    OnChange = edtSearchChange
    TextHint = 'Search'
  end
  object btnPlus: TButton
    Left = 281
    Top = 408
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Plus'
    TabOrder = 3
    OnClick = btnPlusClick
  end
  object btnMoins: TButton
    Left = 200
    Top = 408
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Moins'
    TabOrder = 4
    OnClick = btnMoinsClick
  end
  object btnGo: TButton
    Left = 371
    Top = 408
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Go'
    TabOrder = 5
    OnClick = btnGoClick
  end
  object edtGoLine: TSpinEdit
    Left = 452
    Top = 409
    Width = 121
    Height = 24
    Anchors = [akRight, akBottom]
    MaxValue = 0
    MinValue = 0
    TabOrder = 6
    Value = 0
    OnKeyDown = edtGoLineKeyDown
  end
  object ds1: TDataSource
    Left = 432
    Top = 56
  end
end
