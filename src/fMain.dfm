object FrmMain: TFrmMain
  Left = 0
  Top = 0
  Caption = 'FrmMain'
  ClientHeight = 441
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  Position = poScreenCenter
  OnCreate = FormCreate
  TextHeight = 15
  object btnBaseList: TButton
    Left = 24
    Top = 64
    Width = 75
    Height = 25
    Caption = '&Base List'
    TabOrder = 0
    OnClick = btnBaseListClick
  end
  object cbxTables: TComboBox
    Left = 24
    Top = 19
    Width = 145
    Height = 23
    TabOrder = 1
    Text = 'cbxTables'
  end
  object btnListDatas: TButton
    Left = 24
    Top = 95
    Width = 75
    Height = 25
    Caption = 'List &Datas'
    TabOrder = 2
    OnClick = btnListDatasClick
  end
  object btnSelDatas: TButton
    Left = 24
    Top = 126
    Width = 75
    Height = 25
    Caption = '&Sel Datas'
    TabOrder = 3
    OnClick = btnSelDatasClick
  end
  object ListBox1: TListBox
    Left = 232
    Top = 19
    Width = 153
    Height = 132
    ItemHeight = 15
    TabOrder = 4
  end
  object btnEditDatas: TButton
    Left = 136
    Top = 64
    Width = 75
    Height = 25
    Caption = '&Edit Datas'
    TabOrder = 5
    OnClick = btnEditDatasClick
  end
  inline FraShowGrid1: TFraShowGrid
    Left = 32
    Top = 248
    Width = 529
    Height = 33
    TabOrder = 6
    ExplicitLeft = 32
    ExplicitTop = 248
    ExplicitWidth = 529
    ExplicitHeight = 33
    inherited Edit1: TEdit
      Width = 441
      ExplicitWidth = 441
    end
    inherited Button1: TButton
      Left = 488
      ExplicitLeft = 488
    end
  end
  object Memo1: TMemo
    Left = 64
    Top = 287
    Width = 497
    Height = 66
    Lines.Strings = (
      'Memo1')
    TabOrder = 7
  end
  object btnAutoCompletion: TButton
    Left = 136
    Top = 95
    Width = 75
    Height = 25
    Caption = 'Auto Compl.'
    TabOrder = 8
    OnClick = btnAutoCompletionClick
  end
  object btnUpdDatas: TButton
    Left = 136
    Top = 126
    Width = 75
    Height = 25
    Caption = 'Upd Datas'
    TabOrder = 9
    OnClick = btnUpdDatasClick
  end
end
