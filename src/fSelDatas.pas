unit fSelDatas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, fBaseList, Data.DB, Vcl.StdCtrls,
  JvExStdCtrls, JvEdit, Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid,
  Vcl.Buttons;

type
  TFrmSelDatas = class(TFrmBaseList)
    btnOk: TSpeedButton;
    btnCancel: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure Grid1DblClick(Sender: TObject);
  private
    FId: Variant;
    FKeyName: string;
    function GetId: Variant;
    procedure SetId(const Value: Variant);
  public
    property Id: Variant read GetId write SetId;
    property KeyName: string read FKeyName write FKeyName;
  end;

implementation

{$R *.dfm}

procedure TFrmSelDatas.FormCreate(Sender: TObject);
begin
  inherited;
  FId := -1;
  FKeyName := 'id';
end;

procedure TFrmSelDatas.btnCancelClick(Sender: TObject);
begin
  inherited;
  ModalResult := mrCancel;
end;

procedure TFrmSelDatas.btnOkClick(Sender: TObject);
begin
  inherited;
  ModalResult := mrOk;
end;

function TFrmSelDatas.GetId: Variant;
begin
  if not Assigned(FQry) or KeyName.IsEmpty then
    Exit;

  Result := FQry[KeyName];
end;

procedure TFrmSelDatas.Grid1DblClick(Sender: TObject);
begin
  inherited;
  btnOkClick(btnOk);
end;

procedure TFrmSelDatas.SetId(const Value: Variant);
begin
  if not Assigned(FQry) or KeyName.IsEmpty then
    Exit;

  FQry.Locate(KeyName, Value, []);
end;

end.
