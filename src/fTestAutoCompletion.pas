unit fTestAutoCompletion;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cEditCompletion, Vcl.StdCtrls;

type
  TFrmTestAutoCompl = class(TForm)
    frmAutoComplete: TFraEditCompletion;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    procedure IdChange(Sender: TObject);
  public
  end;

implementation

{$R *.dfm}

{ TFrmTestAutoCompl }

procedure TFrmTestAutoCompl.FormCreate(Sender: TObject);
begin
  frmAutoComplete.Init('Customers','CustomerID','CompanyName','CompanyName',
                    'CustomerID,CompanyName','CompanyName');

  frmAutoComplete.OnIdChange := IdChange;
  frmAutoComplete.SetLabel('Customer');
end;

procedure TFrmTestAutoCompl.IdChange(Sender: TObject);
begin
  Label1.Caption := Format('ID: %s', [VarToStr(frmAutoComplete.Id)])
end;

end.
