unit uMyMessages;

interface

uses
  Vcl.Dialogs;

function Confirm(const AMsg: string): Boolean;
function ConfirmFmt(const AMsg: string; const Args: array of const): Boolean;
procedure MsgInfo(const AMsg: string);
procedure MsgInfoFmt(const AMsg: string; const Args: array of const);
procedure MsgWarning(const AMsg: string);
procedure MsgWarningFmt(const AMsg: string; const Args: array of const);
procedure MsgError(const AMsg: string);
procedure MsgErrorFmt(const AMsg: string; const Args: array of const);

implementation

uses
  System.UITypes,
  System.SysUtils;

function Confirm(const AMsg: string): Boolean;
begin
  Result := MessageDlg(AMsg,
                       TMsgDlgType.mtConfirmation,
                       [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo],
                       0) = mrYes;
end;

function ConfirmFmt(const AMsg: string; const Args: array of const): Boolean;
begin
  Result := Confirm(Format(Amsg, Args));
end;

procedure MsgInfo(const AMsg: string);
begin
  MessageDlg(AMsg,
             TMsgDlgType.mtInformation,
             [TMsgDlgBtn.mbOK],
             0);
end;

procedure MsgInfoFmt(const AMsg: string; const Args: array of const);
begin
  MsgInfo(Format(AMsg, Args));
end;

procedure MsgWarning(const AMsg: string);
begin
  MessageDlg(AMsg,
             TMsgDlgType.mtWarning,
             [TMsgDlgBtn.mbOK],
             0);
end;

procedure MsgWarningFmt(const AMsg: string; const Args: array of const);
begin
  MsgWarning(Format(AMsg, Args));
end;

procedure MsgError(const AMsg: string);
begin
  MessageDlg(AMsg,
             TMsgDlgType.mtError,
             [TMsgDlgBtn.mbOK],
             0);
end;

procedure MsgErrorFmt(const AMsg: string; const Args: array of const);
begin
  MsgError(Format(AMsg, Args));
end;

end.
