unit uParamCols;

interface

uses
  System.Classes,
  Vcl.DBGrids;

type
  TParamCol = class
  private
    FOrder: Integer;
    FName: string;
    FTitle: string;
    FSize: Integer;
    FIsVisible: Boolean;
    FJoinTable: string;
    FJoinKey: string;
    FJoinResult: string;
  public
    property Order: Integer read FOrder write FOrder;
    property Name: string read FName write FName;
    property Title: string read FTitle write FTitle;
    property Size: Integer read FSize write FSize;
    property IsVisible: Boolean read FIsVisible write FIsVisible;
    property JoinTable: string read FJoinTable write FJoinTable;
    property JoinKey: string read FJoinKey write FJoinKey;
    property JoinResult: string read FJoinResult write FJoinResult;
  end;

  TParamCols = array of TParamCol;

  TJoin = class
  private
    FKey: string;
    FFieldName: string;
    FTableName: string;
    FTableKey: string;
    FTableResult: string;
  public
    property Key: string read FKey write FKey;
    property FieldName: string read FFieldName write FFieldName;
    property TableName: string read FTableName write FTableName;
    property TableKey: string read FTableKey write FTableKey;
    property TableResult: string read FTableResult write FTableResult;
  end;

  TTableCols = class
  private
    FCols: TParamCols;
    FTableName: string;
    FJoins: TStrings;
    procedure SetTableName(const Value: string);
    procedure CreateDatas;
    procedure FreeJoinsCols;
  public
    constructor Create;
    destructor Destroy; override;
    function GetSql: string;
    procedure PrepareDBGrid(AColumns: TDBGridColumns);
    property TableName: string read FTableName write SetTableName;
  end;

implementation

uses
  Firedac.Comp.Client,
  System.SysUtils,
  dmDatas;

constructor TTableCols.Create;
begin
  FJoins := TStringList.Create;
end;

destructor TTableCols.Destroy;
begin
  FreeJoinsCols;
  FJoins.Free;
  inherited;
end;

procedure TTableCols.FreeJoinsCols;
var
  i: Integer;
begin
  for i := FJoins.Count-1 downto 0 do
    FJoins.Objects[i].Free;
  FJoins.Clear;
  for i := Length(FCols)-1 downto 0 do
    FCols[i].Free;
  FCols := nil;
end;

procedure TTableCols.SetTableName(const Value: string);
begin
  FTableName := Value;
  CreateDatas;
end;

procedure TTableCols.CreateDatas;
var
  qry: TFDQuery;
  i: Integer;
  str: string;
  join: TJoin;
  lcol: TParamCol;
begin
  qry := MDatas.GetQuery;
  qry.SQL.Text := Format('SELECT * FROM list_cols '
                         + 'WHERE table_name = ''%s'' '
                         + 'ORDER BY field_order;',
                         [FTableName]);
  try
    qry.Open;

    FreeJoinsCols;
    SetLength(FCols, qry.RecordCount);
    i := 0;

    while (not qry.Eof) do
    begin
      FCols[i] := TParamCol.Create;
      FCols[i].Order := qry.FieldByName('field_order').AsInteger;
      FCols[i].Name := qry.FieldByName('field_name').AsString;
      FCols[i].Title := qry.FieldByName('title').AsString;
      FCols[i].Size := qry.FieldByName('field_size').AsInteger;
      FCols[i].IsVisible := qry.FieldByName('visible').AsBoolean;
      FCols[i].JoinTable := qry.FieldByName('join_table').AsString;

      if (not FCols[i].JoinTable.IsEmpty) then
      begin
        FCols[i].JoinKey := qry.FieldByName('join_field').AsString;
        FCols[i].JoinResult := qry.FieldByName('join_result').AsString;

        str := Format('%s-%s', [FCols[i].JoinTable, FCols[i].Name]);
        if (FJoins.IndexOf(str) < 0) then
        begin
          join := TJoin.Create;
          join.Key := str;
          join.FieldName := FCols[i].Name;
          join.TableName := FCols[i].JoinTable;
          join.TableKey := FCols[i].JoinKey;
          join.TableResult := FCols[i].JoinResult;
          FJoins.AddObject(join.Key, join);
        end;
      end;

      i := i + 1;
      qry.Next;
    end;
  finally
    qry.Free;
  end;
end;

function TTableCols.GetSql: string;
var
  strSelect, strJoin: TStringBuilder;
  col: TParamCol;
  i, nb: Integer;
  join: TJoin;
  als: string;
begin
  Result := string.Empty;

  if (Length(FCols) = 0) then
    Result := Format('SELECT * FROM %s;', [FTableName])
  else
  begin
    strSelect := TStringBuilder.Create;
    strJoin := TStringBuilder.Create;

    try
      nb := Length(FCols)-1;
      for i := 0 to nb do
      begin
        col := FCols[i];
        if (col.JoinTable.IsEmpty) then
        begin
          if (not strSelect.ToString.IsEmpty) then
            strSelect.Append(', ');
          strSelect.Append('A1.' + col.Name);
        end;
      end;
      if (nb > 0) then
        strSelect.Append(sLineBreak);

      nb := FJoins.Count-1;
      for i := 0 to nb do
      begin
        join := FJoins.Objects[i] as TJoin;
        als := Format('T%d', [i+1]);
        if (not strSelect.ToString.IsEmpty) then
          strSelect.Append(',');
        strSelect.Append(Format('%s.%s AS %s_%s ',
                          [als,
                           join.TableResult,
                           join.TableName,
                           join.FieldName]));
        strJoin.Append(Format(
                          'LEFT JOIN %s %s ON A1.%s = %1:s.%3:s ',
                          [join.TableName,
                           als,
                           join.FieldName,
                           join.TableKey]));

        strJoin.Append(sLineBreak);
      end;
      if (nb > 0) then
        strSelect.Append(sLineBreak);

      if (not strSelect.ToString.IsEmpty) then
      begin
        strSelect.Insert(0, sLineBreak);
        strSelect.Insert(0, 'SELECT ');

        strSelect.Append(Format('FROM %s A1 ', [FTableName]));
        strSelect.Append(sLineBreak);

        if (not strJoin.ToString.IsEmpty) then
          strSelect.Append(strJoin.ToString);

        strSelect.Append(';');

        Result := strSelect.ToString;
      end;
    finally
      strSelect.Free;
      strJoin.Free;
    end;
  end;
end;

procedure TTableCols.PrepareDBGrid(AColumns: TDBGridColumns);
var
  i: Integer;
  lcol: TParamCol;
begin
  if (Length(FCols) <> AColumns.Count) then
    Exit;

  for i := 0 to AColumns.Count-1 do
    AColumns[i].Visible := False;

  for i := 0 to AColumns.Count-1 do
  begin
    lcol := FCols[i];
    if (lcol.IsVisible) then
    begin
      AColumns[i].Visible := True;
      AColumns[i].Title.Caption := lcol.Title;
      AColumns[i].Width := lcol.Size;
    end;
  end;
end;

end.
