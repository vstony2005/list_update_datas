# List and Update Datas

## Projects

Powered by [Delphi 11 (Alexandria)](https://www.embarcadero.com/products/delphi).

### Delphi 7

Sources : `src\d7\`. Create `src\d7\bin\` and add `sqlite-3.dll` into.

## Library

- [SQLite](https://www.sqlite.org/)
- [jcl](https://github.com/project-jedi/jcl) and [jvcl](https://github.com/project-jedi/jvcl)
- [Northwind](https://gitlab.com/vstony2005/sqlite_northwind)

_For Delphi 7_

- [ZeosLib](https://sourceforge.net/projects/zeoslib/)

_Optionnal_

- [madExcept](http://www.madexcept.com/)

_Not use_

- [Better Translation Manager](https://github.com/CloudDelphi/better-translation-manager)
- [DCPcrypt](https://github.com/Dunhamb4a/DcPCryptV2)
- [Icons](https://www.fatcow.com/free-icons)
- [JAM-Software/Virtual-TreeView](https://github.com/JAM-Software/Virtual-TreeView)
