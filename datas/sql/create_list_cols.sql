
CREATE TABLE list_cols (
    table_name VARCHAR(255) NOT NULL,
    field_order INTEGER NOT NULL,
    field_name VARCHAR(255) NOT NULL,
    field_size INTEGER NOT NULL,
    title VARCHAR(255) NOT NULL,
    visible BIT NOT NULL,
    mandatory BIT NOT NULL,
    join_table VARCHAR(255),
    join_field VARCHAR(255),
    join_result VARCHAR(255)
);
